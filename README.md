# azrs-tracking

Faculty individual project done within the "Software development tools" course.

All tools are tested on project _[HTML editor](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/07-html-editor)_.
Results for each tool are recorded as an individual [issue](https://gitlab.com/efen9/azrs-tracking/-/boards), titled by tool name.
